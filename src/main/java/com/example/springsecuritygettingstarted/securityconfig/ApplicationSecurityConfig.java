package com.example.springsecuritygettingstarted.securityconfig;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@Configuration
@EnableWebSecurity
public class ApplicationSecurityConfig extends WebSecurityConfigurerAdapter {

    /**
     *  .antMatchers("/","index","/css/*","/js/*").permitAll() burda verdiyimiz matcher-a gore
     *  index file-na icaze veririk ki login olmadan giris etsin qalan diger url -ler ucun
     *  authenticated olmasi lazimdi
     *  hal hazirda spring ing oz default login pop up ini goreceyik ve password ise bize console da generasiya olacaq
     *  cunki hele hec bir yerde user ve password ucun kod yazmamisiq
     *
     *  biz customer copntroller i cagiranda bir defe login olmamiz yeterlidi
     *  sonrada diger id lere gore cagiranda tekrar bizden login olmagi teleb etmir
     *  Spring Security bizim ucun Basic Token  JSESSIONID generasiya edir ve biz onunla is gore bilirik
     *  Pop upda cixan username hissesine istediyiniz  bir username yaza bielrsiz , password da ise console da verilen password yazilacaq
     *  default olan versiya ucun bele yaziriq
     */
    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .authorizeRequests()
                .antMatchers("/","index","/css/*","/js/*").permitAll()
                .anyRequest().authenticated()
                .and()
                .httpBasic();
    }
}
